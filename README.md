A bespoke destination weddings and events company with professional consultants & designers serving throughout Turkey. Jaw-dropping backdrops, packages, ideas and venues available.

Address : Istiklal Caddesi, Proper House No:54, 50400 Urgup, Nevsehir, Turkey

Phone : +90 384 341 65 20